jest.mock('../../src/services/fetchResource');

import { GetOaLocationsResponse } from '../../../../lib/interfaces/endpoints/unpaywall';
import { getOpenAccessLocation } from '../../src/services/unpaywall';

describe('getOpenAccessLocation', () => {
  const mockResponse: GetOaLocationsResponse = {
    best_oa_location: {
      host_type: 'publisher',
      is_best: true,
      license: 'cc-by',
      url: 'arbitrary_pdf_url',
      url_for_pdf: 'arbitrary_pdf_url',
    },
    doi: 'arbitrary doi',
    doi_url: 'arbitrary_doi_url',
    is_oa: true,
    journal_is_in_doaj: true,
    journal_is_oa: true,
    journal_name: 'arbitrary journal name',
    oa_locations: [],
    published_date: 'arbitrary date',
    publisher: 'arbitrary publisher',
    title: 'arbitrary title',
    z_authors: [],
  };

  it('should return null if the Unpaywall API encountered an error', () => {
    const mockErrorResponse: GetOaLocationsResponse = {
      HTTP_status_code: 400,
      error: true,
      message: 'Arbitrary error',
    };

    // fetch is mocked by jest-fetch-mock
    fetch.mockResponseOnce(JSON.stringify(mockErrorResponse));

    return expect(getOpenAccessLocation('arbitrary-doi')).resolves.toBeNull();
  });

  it('should return null when no Open Access location could be found by unpaywall', () => {
    const mockEmptyResponse: GetOaLocationsResponse = {
      ...mockResponse,
      best_oa_location: null,
    };

    // fetch is mocked by jest-fetch-mock
    fetch.mockResponseOnce(JSON.stringify(mockEmptyResponse));

    return expect(getOpenAccessLocation('arbitrary-doi')).resolves.toBeNull();
  });

  it('should return a PDF article when available, even when an HTML version is also available', () => {
    const mockPdfResponse: GetOaLocationsResponse = {
      ...mockResponse,
      best_oa_location: {
        ...mockResponse.best_oa_location,
        url: 'some-url',
        url_for_pdf: 'some-other-pdf-url',
      },
    };

    // fetch is mocked by jest-fetch-mock
    fetch.mockResponseOnce(JSON.stringify(mockPdfResponse));

    return expect(getOpenAccessLocation('arbitrary-doi')).resolves.toBe('some-other-pdf-url');
  });

  it('should return a link to a published webpage when no PDF is available', () => {
    const mockPdflessResponse: GetOaLocationsResponse = {
      ...mockResponse,
      best_oa_location: {
        ...mockResponse.best_oa_location,
        url: 'some-url',
        url_for_pdf: null,
      },
    };

    // fetch is mocked by jest-fetch-mock
    fetch.mockResponseOnce(JSON.stringify(mockPdflessResponse));

    return expect(getOpenAccessLocation('arbitrary-doi')).resolves.toBe('some-url');
  });
});
