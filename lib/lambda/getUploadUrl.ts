// This file is excluded from test coverage (through Jest's configuration),
// since it merely calls the AWS SDK API with configuration.
import { S3 } from 'aws-sdk';

const s3 = new S3({
  accessKeyId: process.env.aws_access_key_id,
  region: process.env.aws_region,
  secretAccessKey: process.env.aws_secret_access_key,
  signatureVersion: 'v4',
});

export const getUploadUrl = (bucketId: string, filename: string) => {
  // `mock_bucket` is defined in docker_compose.yml when running locally:
  if (bucketId === 'mock_bucket') {
    return `http://localhost:3000/upload/${Math.random()}`;
  }

  return s3.getSignedUrl('putObject', {
    Bucket: bucketId,
    Expires: 60,
    Key: filename,
  });
};
