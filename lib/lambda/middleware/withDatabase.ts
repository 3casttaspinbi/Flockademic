import * as pgPromise from 'pg-promise';

import { Middleware, Request, Response } from '../faas';

export type Database = pgPromise.IDatabase<any>;
export interface DbContext { database: Database; }

const databases: { [key: string]: Database } = {};

export function withDatabase<RequestBody>(
  connectionString: string,
  schema: string,
  next: Middleware<RequestBody>,
): Middleware<RequestBody> {
  databases[connectionString] = databases[connectionString] || pgPromise({ schema })(connectionString);

  const newMiddleware: Middleware<RequestBody> = async (request: Request<RequestBody>) => {
    const context: Request<RequestBody> & DbContext = {
      ...request,
      database: databases[connectionString],
    };

    const response: Response = await next(context);

    return response;
  };

  return newMiddleware;
}
